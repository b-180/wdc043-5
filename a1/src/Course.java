public class Course {
    private String name, description, startDate, endDate;
    private int seat;
    User instructor;

    public Course() {
    }

    public Course(String name, String description, String startDate, String endDate, int seat, User instructor) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.seat = seat;
        this.instructor = instructor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getInstructor() {
        return instructor.getFirstName();
    }

    public void setInstructor(User instructor) {
        this.instructor = instructor;
    }
}

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Instructor fn", "Instructor ln", "Instructor add", 50);
        Course course1 = new Course();

        course1.setName("HTML 101");
        course1.setDescription("Intro to HTML");
        course1.setStartDate("July 6, 2022 8:00 AM");
        course1.setEndDate("October 1, 2022 5:00 PM");
        course1.setSeat(10);
        course1.setInstructor(user1);

        System.out.printf("User's first name:\n%s\n", user1.getFirstName());
        System.out.printf("User's last name:\n%s\n", user1.getLastName());
        System.out.printf("User's age:\n%s\n", user1.getAge());
        System.out.printf("User's address:\n%s\n", user1.getAddress());
        System.out.printf("Course's name:\n%s\n", course1.getName());
        System.out.printf("Course's description:\n%s\n", course1.getDescription());
        System.out.printf("Course's seats:\n%d\n", course1.getSeat());
        System.out.printf("Course's instructor:\n%s\n", course1.getInstructor());
    }
}
